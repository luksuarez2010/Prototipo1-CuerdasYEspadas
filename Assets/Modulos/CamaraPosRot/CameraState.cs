﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraState : MonoBehaviour {
	public string StateName;
	public bool activa;
	public Transform target;
	public Vector3 localPos, localRot;
	public float velpos = 6f, velrot = 6f;
	public bool UsarAIM;
	public Vector2 AIM;
	private InputAIM aim;
	// Use this for initialization
	void Start () {
		aim = FindObjectOfType<InputAIM>();
	}
	
	// Update is called once per frame
	void Update () {
		if(activa){
			if(!UsarAIM){
				Vector3 miLocalPos = target.position + target.forward * localPos.z + target.right * localPos.x + target.up * localPos.y;
				transform.position = Vector3.Lerp(transform.position, miLocalPos, velpos * Time.deltaTime);
				transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation * Quaternion.Euler(localRot), velrot * Time.deltaTime);
			}else{
				AIM += aim.AIM * Time.deltaTime * 0.1f;
				Vector3 miLocalPos = target.position + target.forward * localPos.z + target.right * localPos.x + target.up * localPos.y;
				transform.position = Vector3.Lerp(transform.position, miLocalPos, velpos * Time.deltaTime);
				transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation * Quaternion.Euler(localRot) * Quaternion.Euler(new Vector3(-AIM.y, AIM.x, 0f)), velrot * Time.deltaTime);
			}
		}
	}
}
