﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraStateController : MonoBehaviour {
	public List<CameraState> States;
	public int DefaultState = 0;

	private static CameraStateController instance;
	public static CameraStateController Instance{
		get {
			if(!instance){
				instance = FindObjectOfType<CameraStateController>();
			}
			return instance;
		}
	}

	// Use this for initialization
	void Start () {
		StateChange(DefaultState);
	}

	public void StateChange(int id){
		for(int s = 0; s < States.Count; s++){
			if(s != id)
				States[s].activa = false;
			else{
				States[s].activa = true;
			}
		}
	}
}
