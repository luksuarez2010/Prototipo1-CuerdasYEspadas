﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour {
	private Rigidbody miRigid;
	public float vel = 50f, tiempoDeVida = 2f;
	private PoolObjects po;

	void Awake(){
		miRigid = GetComponent<Rigidbody>();
		po = FindObjectOfType<PoolObjects>();
	}

	void OnEnable () {
		Invoke("Impulsar", 0.01f);
	}

	void Impulsar(){
		miRigid.AddForce(transform.forward * vel);
		StartCoroutine(VerificarSupervivencia());
	}

	IEnumerator VerificarSupervivencia(){
		yield return new WaitForSeconds(tiempoDeVida);
		po.Destruir(this.gameObject);
	}

}
