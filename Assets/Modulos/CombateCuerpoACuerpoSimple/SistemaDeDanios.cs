﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;

public class SistemaDeDanios : MonoBehaviour {
	[System.Serializable]
	public class DanioType{
		public string tag;
		public int danio;
	}

	public int vida = 100;
	public Slider sliderVida;
	public bool SeteaPuntosAlMorir = true;

	public bool DanioPorVelocidad = true;
	public float velocidadDanio = 15f;
	public bool danioPorColision;
	public List<DanioType> danio;
	public UnityEvent muerte, danioEv;
	// Use this for initialization
	void Start () {
		if(sliderVida){
			sliderVida.maxValue = vida;
			sliderVida.value = vida;
		}
	}

	void OnCollisionEnter(Collision col){
		if(DanioPorVelocidad){
			Rigidbody colR = col.gameObject.GetComponent<Rigidbody>();
			if(!colR){
				colR = GetComponent<Rigidbody>();
			}
			float velR = colR.velocity.sqrMagnitude * 0.1f;

			if(velR > velocidadDanio){
					Danio((int)velR);
			}
		}
		if(danioPorColision){
			foreach(DanioType d in danio){
				if(d.tag == col.gameObject.tag){
					Danio(d.danio);
				}
			}
		}
	}

	void Danio (int d){
		vida -= d;

		if(sliderVida){
			int val = vida;
			sliderVida.value = val;
		}
		if(vida <= 0){
			muerte.Invoke();
			if(SeteaPuntosAlMorir)
				Puntaje.Instance.puntaje(vida*-1);
		}else{
			danioEv.Invoke();
		}
		if(vida < 0) vida = 0; // lo pongo aca porque arriba uso el -vida para sumar puntos, y lo pongo en cero por si gana puntos de vida antes de morir
	}
}
