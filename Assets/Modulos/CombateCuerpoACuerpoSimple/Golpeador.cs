﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Golpeador : MonoBehaviour {
	public string triggerAtaque = "ataque"; 
	public UnityEvent golpe;
	private SphereCollider col;
	public Animator anim;

	// Use this for initialization
	void Start () {
		col = gameObject.AddComponent<SphereCollider>();
		col.isTrigger = true;
		col.radius = 4f;
	}

	// Update is called once per frame
	void OnTriggerStay (Collider col) {
		if(col.GetComponent<Golpeado>()){
			CameraStateController.Instance.StateChange(1);
			for(int t = 0; t < Input.touches.Length; t++){
				if(Input.touches[t].phase == TouchPhase.Began){
					atacar();
				}
			}
		}
	}
	void OnTriggerExit (Collider col) {
		if(col.GetComponent<Golpeado>()){
			CameraStateController.Instance.StateChange(0);
		}
	}

	public void atacar(){
		print("Atacando");
		if(anim){
			anim.SetTrigger(triggerAtaque);
		}
		golpe.Invoke();
	}
}
