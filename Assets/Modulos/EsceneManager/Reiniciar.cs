﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Reiniciar : MonoBehaviour {
	public bool ReiniciarEnActivacion;

	void Start(){
		if(ReiniciarEnActivacion)
			ReiniciarEscena();
	}
	public void ReiniciarEscena(){
		SceneManager.LoadSceneAsync(0);
	}
}
