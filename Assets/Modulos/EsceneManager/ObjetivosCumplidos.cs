﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class ObjetivosCumplidos : MonoBehaviour {
	public string[] objetivoPorTipo; //ejemplo: Enemigo.cs = "Enemigo"
	public List<GameObject> objetivos;
	public UnityEvent finObjetivos;

	// Use this for initialization
	void Start () {
		MonoBehaviour[] allSc = FindObjectsOfType<MonoBehaviour>();
		for(int c = 0; c < allSc.Length; c++){
			for(int t = 0; t < objetivoPorTipo.Length; t++){
				if(allSc[c].GetType().ToString() == objetivoPorTipo[t]){
					objetivos.Add(allSc[c].gameObject);
				}
			}
		}
		InvokeRepeating("miUpdate", 1f, 1f);
	}
	
	// Update is called once per frame
	void miUpdate () {
		bool flag = true;
		foreach(GameObject o in objetivos){
			if(o.activeSelf){
				flag = false;
			}
		}
		if(flag){
			finObjetivos.Invoke();
		}
	}
}
