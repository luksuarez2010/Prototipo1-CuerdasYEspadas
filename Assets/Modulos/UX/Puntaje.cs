﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;

public class Puntaje : MonoBehaviour {
	public int puntuacion;
	private Puntaje instance;
	public static Puntaje Instance;
	public Text punt;
	public UnityEvent cuandoDejaDeContar;

	void Start(){
		instance = FindObjectOfType<Puntaje>();
		if(instance) Instance = instance;
	}

	public void puntaje(int p){
		StartCoroutine(puntajeAnim(p));
	}
	IEnumerator puntajeAnim(int p){
		int miP = 0;
		while(miP <= p){
			puntuacion ++;
			miP ++;
			punt.text = puntuacion.ToString();
			yield return new WaitForSeconds(0.001f);
		}
		yield return new WaitForSeconds(0.1f);
		cuandoDejaDeContar.Invoke();
	}
	public void restarPuntaje(int p){
		puntuacion -= p;
		punt.text = puntuacion.ToString();
	}
}
