﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapaAereo : MonoBehaviour {
	[System.Serializable]
	public class MAPID{
		public GameObject graficID;
		public string scriptID;
	}
	public List<MAPID> mapID;

	// Use this for initialization
	void Start () {
		MonoBehaviour[] obj = FindObjectsOfType<MonoBehaviour>();

		for(int id = 0; id < mapID.Count; id++){			
			for(int o = 0; o < obj.Length; o++){
				
				if(obj[o].GetType().ToString() == mapID[id].scriptID){
					GameObject ga = Instantiate(mapID[id].graficID, obj[o].transform.position, Quaternion.Euler(0f,0f,0f));
					ga.transform.parent = obj[o].gameObject.transform;
				}
			}
		}	
	}
}
