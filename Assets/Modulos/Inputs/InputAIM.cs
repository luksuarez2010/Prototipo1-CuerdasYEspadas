﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class InputAIM : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler {
	public float tiempo;
	public bool cuenta;
	public float tiempoParaDisparo = 0.3f;
	public Image Mira, fondoInputAIM;

	//AIM
	private Vector2 posDown;
	public Vector2 AIM;

	void Update(){
		if(cuenta){
			tiempo += Time.deltaTime;
		}
	}


	public void OnPointerDown(PointerEventData ev){
		cuenta = true;
		posDown = ev.position;
		if(fondoInputAIM)
			fondoInputAIM.gameObject.SetActive(true);
	}

	public void OnPointerUp(PointerEventData ev){
		cuenta = false;
		if(tiempo <= tiempoParaDisparo){
			disparo(ev.position);
		}
		tiempo = 0f;
		AIM = Vector2.zero;
		if(fondoInputAIM)
			fondoInputAIM.gameObject.SetActive(false);
	}

	// Update is called once per frame
	public void OnDrag (PointerEventData ev) {
		AIM = ev.position - posDown;
	}

	public delegate void DisparoDel(Vector3 dir);
	public static DisparoDel disparo;

	public void Disparo(Vector3 pos){
		//print("disparo " + pos);
		if(Mira){
			Mira.gameObject.SetActive(true);
			Mira.rectTransform.position = pos;
			StartCoroutine(DesactivarMira());
		}
	}
	void Awake(){
		disparo += Disparo;
		if(Mira)
			Mira.gameObject.SetActive(false);
		if(fondoInputAIM)
			fondoInputAIM.gameObject.SetActive(false);
	}
	IEnumerator DesactivarMira(){
		yield return new WaitForSeconds(1f);
		Mira.gameObject.SetActive(false);
	}
}
