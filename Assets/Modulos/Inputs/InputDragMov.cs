﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class InputDragMov : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {
	public Image puntoInicio, puntoFinal;
	public Vector2 Dir;
	Vector3 posPuntoIni, posPuntoFin;
	public float rangoMaximoMovimiento = 100f;
	Vector2 movPF;

	void Start(){
		posPuntoIni = puntoInicio.rectTransform.position;
		posPuntoFin = puntoFinal.rectTransform.position;
	}

	public void OnPointerDown(PointerEventData ev){
		puntoInicio.rectTransform.position = new Vector3(ev.position.x, ev.position.y, 0f);
	}

	public void OnPointerUp(PointerEventData ev){
		puntoInicio.rectTransform.position = posPuntoIni; // vuelve a pose inicial
		puntoFinal.rectTransform.position = posPuntoFin; // vuelve a pose inicial
		Dir = Vector2.zero;
	}

	public void OnDrag(PointerEventData ev){
		if(ev.position.x - puntoInicio.rectTransform.position.x > -rangoMaximoMovimiento && ev.position.x - puntoInicio.rectTransform.position.x < rangoMaximoMovimiento){ movPF.x = ev.position.x;}
		else{
			if(ev.position.x - puntoInicio.rectTransform.position.x <= -rangoMaximoMovimiento) movPF.x = puntoInicio.rectTransform.position.x  - rangoMaximoMovimiento;
			if(ev.position.x - puntoInicio.rectTransform.position.x >= rangoMaximoMovimiento) movPF.x = puntoInicio.rectTransform.position.x + rangoMaximoMovimiento;
		}
		if(ev.position.y - puntoInicio.rectTransform.position.y > -rangoMaximoMovimiento && ev.position.y - puntoInicio.rectTransform.position.y < rangoMaximoMovimiento){ movPF.y = ev.position.y;}
		else{
			if(ev.position.y - puntoInicio.rectTransform.position.y < -rangoMaximoMovimiento) movPF.y = puntoInicio.rectTransform.position.y - rangoMaximoMovimiento;
			if(ev.position.y - puntoInicio.rectTransform.position.y > rangoMaximoMovimiento) movPF.y = puntoInicio.rectTransform.position.y + rangoMaximoMovimiento;
		}

		puntoFinal.rectTransform.position = new Vector3(movPF.x, movPF.y, 0f);

		Dir.x = puntoFinal.rectTransform.localPosition.x / rangoMaximoMovimiento;
		Dir.y = puntoFinal.rectTransform.localPosition.y / rangoMaximoMovimiento;
	}
}
