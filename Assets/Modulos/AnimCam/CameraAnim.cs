﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnim : MonoBehaviour {
	private Animator anim;

	private static CameraAnim instance;
	public static CameraAnim Instance{
		get {
			if(!instance){
				instance = FindObjectOfType<CameraAnim>();
			}
			return instance;
		}
	}

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}
	
	public void SetAimator(int id){
		anim.SetTrigger("AnimSet");
		anim.SetInteger("ID", id);
	}
}
