﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILookAtCamera : MonoBehaviour {
	private Transform target;
	public bool visible = false;
	public GameObject UIVisible;
	public Transform PlayerDir;

	// Use this for initialization
	void Start () {
		target = Camera.main.transform;

		PlayerDir = FindObjectOfType<PlayerMovPJ>().transform;

	}
	
	// Update is called once per frame
	void Update () {

		if(Vector3.Angle(PlayerDir.right, transform.position - PlayerDir.position) < 90f && Vector3.Distance(transform.position, PlayerDir.position) > 4f){
			visible = true;
		}
		else{
			visible = false;
		}



		if(visible){
			if(!UIVisible.activeSelf)
				UIVisible.SetActive(true);
			
			transform.LookAt(target);
		}
		else{
			if(UIVisible.activeSelf)
				UIVisible.SetActive(false);
		}
	}
}
