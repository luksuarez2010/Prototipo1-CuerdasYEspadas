﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIConpras : MonoBehaviour {
	public Slider slider;
	public Puntaje puntaje;
	public SistemaDeDanios vidaPlayer;

	public GameObject DanioPorColision;

	public GameObject Mapa;

	public void ComprarVida(){
		if(slider){
			float necesidad = slider.maxValue - slider.value;
			if(puntaje.puntuacion >= necesidad){
				puntaje.restarPuntaje((int)necesidad);
				slider.value += necesidad;
				vidaPlayer.vida += (int)necesidad;
			}else{
				puntaje.restarPuntaje(puntaje.puntuacion);
				slider.value += puntaje.puntuacion;
				vidaPlayer.vida += puntaje.puntuacion;
			}
		}
	}
	public void ObtenerVida(int v){
		vidaPlayer.vida += v;
		slider.value += v;
	}

	public void ComprarDanioPorCollision(){
		if(puntaje.puntuacion >= 1000){
			puntaje.restarPuntaje(1000);
			DanioPorColision.tag = "Finish";
		}
	}

	public void ComprarMapa(){
		if(puntaje.puntuacion >= 500){
			puntaje.restarPuntaje(500);
			Mapa.SetActive(true);
		}
	}
}
