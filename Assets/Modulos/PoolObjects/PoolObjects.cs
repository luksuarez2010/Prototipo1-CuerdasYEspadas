﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolObjects : MonoBehaviour {

	[System.Serializable]
	public class TipoObj{
		public GameObject obj;
		public List<GameObject> instanciados;
	}
	public TipoObj[] tipoObj;

	public Vector3 posPool = new Vector3(0f,-1000f,0f);


	private List<GameObject> padres = new List<GameObject>();

	void Awake(){
		for(int o = 0; o < tipoObj.Length; o++){
			GameObject padre = new GameObject();
			padre.name = tipoObj[o].obj.name + "_Padre";
			padre.transform.parent = transform;
			padres.Add(padre);
		}
	}

	public void Intanciar(int tipObj, Vector3 pos, Quaternion rot){
		GameObject instanciado = null;
		bool flagInst = true;
		for(int i = 0; i < tipoObj[tipObj].instanciados.Count; i++){
			if(tipoObj[tipObj].instanciados[i].activeSelf == false){
				flagInst = false;
				instanciado = tipoObj[tipObj].instanciados[i];
			}
		}
		if(flagInst){
			instanciado = Instantiate(tipoObj[tipObj].obj);
			PoolID miPoolID = instanciado.AddComponent<PoolID>();
			miPoolID.ID = tipObj;
			instanciado.transform.parent = padres[tipObj].transform;
			tipoObj[tipObj].instanciados.Add(instanciado);
		}


		instanciado.transform.position = pos;
		instanciado.transform.rotation = rot;
		instanciado.SetActive(true);
	}

	public void Destruir (GameObject o){
		o.SetActive(false);
	}
}
