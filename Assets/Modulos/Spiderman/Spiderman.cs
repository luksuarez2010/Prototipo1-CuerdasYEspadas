﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Spiderman : MonoBehaviour {
	public SpringJoint joint;
	public Telarania Enganche;
	public List<Rigidbody> dobleses;
	public Transform disparador;
	public float velocidadDeDisparo = 50f;
	public Vector3 mousePosition;
	public float springValue = 10, distanciaDeDisparo = 100f;
	public LineRenderer cadena;
	public Animator anim;
	private Rigidbody miRigid;
	// Use this for initialization
	void Start () {
		Enganche.miSpider = this;
		miRigid = GetComponent<Rigidbody>();
		InputAIM.disparo += LanzaEnganche;
	}

	void Update () {
		//cadena line renderer
		cadena.SetPosition(0, transform.position);
		cadena.SetPosition(1, Enganche.transform.position);

		//enganche
		if(joint){
			if(Enganche.disparado){
				joint.spring = 0f;
				anim.SetBool("Volando", false);
			}else{
				joint.spring = springValue;
				if(Vector3.Distance(transform.position, Enganche.transform.position) > 15f){
					CameraAnim.Instance.SetAimator(0);
				}
				Vector3 posEng = new Vector3(Enganche.transform.position.x, transform.position.y, Enganche.transform.position.z);
				Quaternion lookAt = Quaternion.LookRotation(posEng - transform.position);
				transform.rotation = Quaternion.Lerp(transform.rotation, lookAt, Time.deltaTime * 4f);
				if(miRigid.velocity.magnitude > 20f){
					anim.SetBool("Volando", true);
					anim.SetBool("Apuntando", false);
				}else{ 
					anim.SetBool("Volando", false);
				}
				anim.SetBool("Apuntando", false);
			}
		}

		//acciones
		if(Vector3.Distance(transform.position, Enganche.transform.position) < 5f && !Enganche.disparado){
			Destroy(joint);
			Enganche.transform.position = disparador.position;
			anim.SetBool("Volando", false);
			Enganche.rend.enabled = false;
			Enganche.rend2.enabled = false;
		}
	}
	
	public void LanzaEnganche(Vector3 rayPos){
		//crear joint
		if(!joint){
			joint = gameObject.AddComponent<SpringJoint>();
			joint.spring = 10f;
			joint.damper = 0.2f;
			joint.anchor = Vector3.zero;
			joint.autoConfigureConnectedAnchor = false;
			joint.connectedAnchor = Vector3.zero;
			joint.connectedBody = Enganche.GetComponent<Rigidbody>();
		}

		//disparar
		RaycastHit hit;
		Ray rayo = Camera.main.ScreenPointToRay(rayPos);
		if(Physics.Raycast(rayo, out hit, distanciaDeDisparo)){
			disparador.LookAt(hit.point);
			Enganche.transform.position = disparador.position;
			Enganche.transform.rotation = disparador.rotation;
			Enganche.SetVel(velocidadDeDisparo);
			Enganche.disparado = true;
			joint.spring = springValue;
			//anim.SetTrigger("ataque");
			anim.SetBool("Apuntando", true);
			Enganche.rend.enabled = true;
			Enganche.rend2.enabled = true;
		}
	}
	void OnDestroy(){
		InputAIM.disparo -= LanzaEnganche;
	}
}
