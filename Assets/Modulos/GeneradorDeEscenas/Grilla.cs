﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grilla : MonoBehaviour {
	public int largo = 10, ancho = 10;
	public float EspacioEntrePuntos = 2f;
	public List<GameObject> puntosDeGrilla;

	public bool TieneRio;
	public List<GameObject> rios;
	public GameObject test;
	public List<GameObject> Llanos, Montanias, Puentes;

	public int probabilidadDeNoMontania = 5;
	public int probabilidadDeNoPuente = 3;
	public float variacionMaximaDeAlturaMontania, variacionMaximaDeAlturaLlano;

	// Use this for initialization
	void Start () {
		Crear();
	}

	public void Crear(){
		#region paso 0 - creacion de grilla
		GameObject puntosGrilla = new GameObject();
		puntosGrilla.transform.parent = transform;
		puntosGrilla.name = "PuntosGrilla";

		for(int l = 0; l < largo; l++){
			for(int a = 0; a < ancho; a++){
				GameObject miGO = new GameObject();
				miGO.transform.position = transform.position + transform.right * (EspacioEntrePuntos * l) + transform.forward * (EspacioEntrePuntos * a);
				miGO.transform.parent = puntosGrilla.transform;
				miGO.name = "L"+l+"A"+a;
				puntosDeGrilla.Add(miGO);
			}
		}
		#endregion
		#region paso 1 - creacion de rio
		if(TieneRio){
			int id = Random.Range(0, largo);
			for(int g = 0; g < puntosDeGrilla.Count; g++){
				if(puntosDeGrilla[g].name == "L"+id+"A0"){
					crearRio(puntosDeGrilla[g]);
				}
			}
		}
		#endregion
	}
	#region paso 1 - creacion del rio (paso a paso)
	void crearRio(GameObject punto){
		List<GameObject> posibles = new List<GameObject>();
		rios.Add(punto);
		for(int p = 0; p < puntosDeGrilla.Count; p++){
			float Distancia = Vector3.Distance(punto.transform.position, puntosDeGrilla[p].transform.position);
			if(Distancia <= EspacioEntrePuntos*1.6f){
				float Angulo = Vector3.Angle(punto.transform.forward, puntosDeGrilla[p].transform.position - punto.transform.position);
				if(Angulo < 80f){
					posibles.Add(puntosDeGrilla[p]);
				}
			}
		}
		if(posibles.Count > 0){
			int posibleID = Random.Range(0, posibles.Count);
			crearRio(posibles[posibleID]);
		}else{
			CreaccionEscenicaConAssets();
		}
	}
	#endregion

	#region paso 2 - creacion de escena con assets modulares
	void CreaccionEscenicaConAssets(){
		#region padre rio
		GameObject rioGOs = new GameObject();
		rioGOs.transform.parent = transform;
		rioGOs.name = "Rio";
		#endregion
		#region padre llano
		GameObject llanoGOs = new GameObject();
		llanoGOs.transform.parent = transform;
		llanoGOs.name = "Llano";
		#endregion
		#region padre montania
		GameObject montGOs = new GameObject();
		montGOs.transform.parent = transform;
		montGOs.name = "Montania";
		#endregion
		#region padre puente
		GameObject puenteGOs = new GameObject();
		puenteGOs.transform.parent = transform;
		puenteGOs.name = "Puente";
		#endregion

		for(int r = 0; r < rios.Count; r++){
			puntosDeGrilla.Remove(rios[r]);
			GameObject ir = Instantiate(test, rios[r].transform.position, Quaternion.identity);
			ir.transform.parent = rioGOs.transform;

			if(Random.Range(0, probabilidadDeNoPuente) == 1){
				GameObject i = Instantiate(Puentes[Random.Range(0, Puentes.Count)], rios[r].transform.position, Quaternion.identity); //puentes
				i.transform.parent = puenteGOs.transform;
			}
		}
		for(int g = 0; g < puntosDeGrilla.Count; g++){
			if(Random.Range(0, probabilidadDeNoMontania) == 1){
				GameObject i = Instantiate(Montanias[Random.Range(0, Montanias.Count)], puntosDeGrilla[g].transform.position + Vector3.up * Random.Range(-variacionMaximaDeAlturaMontania, 0f), 
					           Quaternion.Euler(new Vector3(0,Random.Range(-360f, 360f), 0f))); //montania
				i.transform.parent = montGOs.transform;
			}else{
				GameObject i = Instantiate(Llanos[Random.Range(0, Llanos.Count)], puntosDeGrilla[g].transform.position + Vector3.up * Random.Range(0, variacionMaximaDeAlturaLlano), 
					           Quaternion.Euler(new Vector3(0,Random.Range(-360f, 360f), 0f))); //llano
				i.transform.parent = llanoGOs.transform;
			}
		}
		Destroy(this);
	}
	#endregion
}
