﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vegetacion : MonoBehaviour {
	public List<GameObject> instanciadores;
	public List<GameObject> modulos3D, modulos3DSuperpuestos;
	public int probabilidadDeNoInstanciado = 3;

	// Use this for initialization
	void Start () {
		Generar();
	}

	void Generar(){
		for(int i = 0; i < instanciadores.Count; i++){
			if(Random.Range(0, probabilidadDeNoInstanciado) == 1){
				GameObject m = Instantiate(modulos3D[Random.Range(0, modulos3D.Count)], instanciadores[i].transform.position, instanciadores[i].transform.rotation * Quaternion.Euler(0,Random.Range(-360f, 360f),0));
				m.transform.parent = instanciadores[i].transform;
			}
			else if(Random.Range(0, probabilidadDeNoInstanciado) == 1){
				GameObject m = Instantiate(modulos3DSuperpuestos[Random.Range(0, modulos3DSuperpuestos.Count)], instanciadores[i].transform.position, instanciadores[i].transform.rotation * Quaternion.Euler(0,Random.Range(-360f, 360f),0));
				m.transform.parent = instanciadores[i].transform;
			}
		}
		Destroy(this);
	}
}
