﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMovPJ : MonoBehaviour {
	private MovimientoPersonaje movPJ;
	private InputDragMov minput;
	private InputAIM minputAIM;
	public float MaxAIM = 45f;
	// Use this for initialization
	void Start () {
		movPJ = GetComponent<MovimientoPersonaje>();
		minput = FindObjectOfType<InputDragMov>();
		minputAIM = FindObjectOfType<InputAIM>();
	}
	
	// Update is called once per frame
	void Update () {
		movPJ.velF = minput.Dir.y;
		movPJ.velR = minput.Dir.x;
		movPJ.velRot = minputAIM.AIM.x * Time.deltaTime * 0.2f;

		movPJ.AIM += minputAIM.AIM * Time.deltaTime * 0.5f;
		movPJ.AIM.x = Mathf.Clamp(movPJ.AIM.x, -MaxAIM, MaxAIM);
		movPJ.AIM.y = Mathf.Clamp(movPJ.AIM.y, -MaxAIM, MaxAIM);
	}
}
