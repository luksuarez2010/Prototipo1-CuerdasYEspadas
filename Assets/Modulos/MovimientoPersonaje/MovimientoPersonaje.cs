﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPersonaje : MonoBehaviour {
	public bool usar = true;
	[Range(-1f, 1f)]
	public float velF, velR, velRot;// "inputs"
	public bool RotVelR = true; // en vez de moverse rota con el input velR 
	[HideInInspector]
	public Rigidbody miRigid;
	public float maxVelMov = 5f, maxVelRot = 5f;
	public Vector2 AIM;
	public bool Escalar;
	public Transform Escalador, EscaladorComprobadorDeNecesidad;
	[HideInInspector]
	public bool Escalando;
	private bool necesitaEscalar;

	// Use this for initialization
	void Start () {
		miRigid = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		if(usar){
			//movimiento
			transform.position += transform.forward * maxVelMov * velF * Time.deltaTime;
			if(RotVelR)	{
				transform.rotation *= Quaternion.Euler(new Vector3(0f, maxVelRot * velR, 0f));
			}else{
				transform.position += transform.right * maxVelMov * velR * Time.deltaTime;
				transform.rotation *= Quaternion.Euler(new Vector3(0f, maxVelRot * velRot, 0f));
			}
		}
		if(Escalar){
			RaycastHit hitGenesis;
			if(Physics.Raycast(Escalador.transform.position, EscaladorComprobadorDeNecesidad.transform.forward, out hitGenesis, 1f)){
				necesitaEscalar = true;
			}
			if(necesitaEscalar){
				RaycastHit hit;
				if(Physics.Raycast(Escalador.transform.position, Escalador.transform.forward, out hit, 1f)){
					if(velF > 0){
						Escalando = true;
						transform.position += transform.up * maxVelMov * Time.deltaTime;
					}
					else {
						if(Escalando){
							Escalando = false;
							transform.position += transform.up * 0.3f + transform.forward * 0.3f;
							necesitaEscalar = false;
						}
					}
				}else {
					Escalando = false;
					necesitaEscalar = false;
				}
			}
		}
	}
}
