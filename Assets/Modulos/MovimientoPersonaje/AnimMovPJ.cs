﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimMovPJ : MonoBehaviour {
	private Animator anim;
	private MovimientoPersonaje padre;
	public float velF, velR;
	public float AIMUp, AIMDer;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		padre = transform.parent.GetComponent<MovimientoPersonaje>();
	}
	
	// Update is called once per frame
	void Update () {
		velF = padre.velF; //suavizarlo con la velocidad del rigidbody
		velR = padre.velR;
		AIMUp = padre.AIM.y;
		AIMDer = padre.AIM.x;

		anim.SetFloat("velF", velF);
		anim.SetFloat("velR", velR);
		anim.SetFloat("AIMUp", AIMUp);
		//anim.SetFloat("AIMDer", AIMDer);

		anim.SetBool("Escalando", padre.Escalando);
		if(padre.Escalando){
			anim.SetLayerWeight(1, 0f);
		}else{
			anim.SetLayerWeight(1, 1f);
		}
	}
}
