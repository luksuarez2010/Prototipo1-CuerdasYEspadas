﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicaSegunLoQuePase : MonoBehaviour {
	public Transform Player;
	private Deteccion[] Enemigos;
	public AudioSource Tension;

	// Use this for initialization
	void Start () {
		Enemigos = FindObjectsOfType<Deteccion>();
		InvokeRepeating("miUpdate", 0.3f, 0.3f);
	}
	
	// Update is called once per frame
	void miUpdate () {
		float MaxDist = Mathf.Infinity;
		for(int e = 0; e < Enemigos.Length; e++){
			if(Vector3.Distance(Enemigos[e].transform.position, Player.position) < MaxDist){
				MaxDist = Vector3.Distance(Enemigos[e].transform.position, Player.position);
				Tension.volume = 10f - MaxDist*0.5f;
			}
		}
	}
}
