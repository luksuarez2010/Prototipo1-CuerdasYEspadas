﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;
using UnityEngine;

public class VideosPublicitarios : MonoBehaviour {
	public string idAndroid, idIOS;
	private string gameID;
	public string placementID = "rewardedVideo";

	// Use this for initialization
	void Start () {
		#if UNITY_ANDROID
		gameID = idAndroid;
		#elif UNITY_IOS
		gameID = idIOS;
		#endif
		if (Advertisement.isSupported) {
			Advertisement.Initialize (gameID, true);
		}
	}
	
	public void Publicidad(){
		if(Advertisement.IsReady(placementID)) {
			Advertisement.Show(placementID);
		}
	}
	void ShowAd ()
	{
		ShowOptions options = new ShowOptions();
		options.resultCallback = HandleShowResult;

		Advertisement.Show(placementID, options);
	}

	void HandleShowResult (ShowResult result)
	{
		if(result == ShowResult.Finished) {
			Debug.Log("Video completed - Offer a reward to the player");

		}else if(result == ShowResult.Skipped) {
			Debug.LogWarning("Video was skipped - Do NOT reward the player");

		}else if(result == ShowResult.Failed) {
			Debug.LogError("Video failed to show");
		}
	}
}
