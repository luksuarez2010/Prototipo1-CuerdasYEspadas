%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: AIM
  m_Mask: 00000000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Cube
    m_Weight: 1
  - m_Path: Cube_001
    m_Weight: 1
  - m_Path: rig
    m_Weight: 1
  - m_Path: rig/root
    m_Weight: 1
  - m_Path: rig/root/Cam_Position
    m_Weight: 1
  - m_Path: rig/root/ORG-hips
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-spine
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-neck
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-neck/ORG-head
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-neck/ORG-head/ORG-Ojo_L
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-neck/ORG-head/ORG-Ojo_R
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/DEF-f_index_01_L_01
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/DEF-palm_01_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/DEF-thumb_01_L_01
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/ORG-f_index_01_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/ORG-f_index_01_L/DEF-f_index_01_L_02
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/ORG-f_index_01_L/ORG-f_index_02_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/ORG-f_index_01_L/ORG-f_index_02_L/DEF-f_index_02_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/ORG-f_index_01_L/ORG-f_index_02_L/ORG-f_index_03_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/ORG-f_index_01_L/ORG-f_index_02_L/ORG-f_index_03_L/DEF-f_index_03_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/ORG-thumb_01_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/ORG-thumb_01_L/DEF-thumb_01_L_02
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/ORG-thumb_01_L/ORG-thumb_02_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/ORG-thumb_01_L/ORG-thumb_02_L/DEF-thumb_02_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/ORG-thumb_01_L/ORG-thumb_02_L/ORG-thumb_03_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_01_L/ORG-thumb_01_L/ORG-thumb_02_L/ORG-thumb_03_L/DEF-thumb_03_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_02_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_02_L/DEF-f_middle_01_L_01
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_02_L/DEF-palm_02_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_02_L/ORG-f_middle_01_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_02_L/ORG-f_middle_01_L/DEF-f_middle_01_L_02
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_02_L/ORG-f_middle_01_L/ORG-f_middle_02_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_02_L/ORG-f_middle_01_L/ORG-f_middle_02_L/DEF-f_middle_02_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_02_L/ORG-f_middle_01_L/ORG-f_middle_02_L/ORG-f_middle_03_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_02_L/ORG-f_middle_01_L/ORG-f_middle_02_L/ORG-f_middle_03_L/DEF-f_middle_03_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_03_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_03_L/DEF-f_ring_01_L_01
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_03_L/DEF-palm_03_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_03_L/ORG-f_ring_01_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_03_L/ORG-f_ring_01_L/DEF-f_ring_01_L_02
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_03_L/ORG-f_ring_01_L/ORG-f_ring_02_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_03_L/ORG-f_ring_01_L/ORG-f_ring_02_L/DEF-f_ring_02_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_03_L/ORG-f_ring_01_L/ORG-f_ring_02_L/ORG-f_ring_03_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_03_L/ORG-f_ring_01_L/ORG-f_ring_02_L/ORG-f_ring_03_L/DEF-f_ring_03_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_04_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_04_L/DEF-f_pinky_01_L_01
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_04_L/DEF-palm_04_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_04_L/ORG-f_pinky_01_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_04_L/ORG-f_pinky_01_L/DEF-f_pinky_01_L_02
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_04_L/ORG-f_pinky_01_L/ORG-f_pinky_02_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_04_L/ORG-f_pinky_01_L/ORG-f_pinky_02_L/DEF-f_pinky_02_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_04_L/ORG-f_pinky_01_L/ORG-f_pinky_02_L/ORG-f_pinky_03_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-palm_04_L/ORG-f_pinky_01_L/ORG-f_pinky_02_L/ORG-f_pinky_03_L/DEF-f_pinky_03_L
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/Gun_Position
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/DEF-f_index_01_R_01
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/DEF-palm_01_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/DEF-thumb_01_R_01
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/ORG-f_index_01_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/ORG-f_index_01_R/DEF-f_index_01_R_02
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/ORG-f_index_01_R/ORG-f_index_02_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/ORG-f_index_01_R/ORG-f_index_02_R/DEF-f_index_02_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/ORG-f_index_01_R/ORG-f_index_02_R/ORG-f_index_03_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/ORG-f_index_01_R/ORG-f_index_02_R/ORG-f_index_03_R/DEF-f_index_03_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/ORG-thumb_01_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/ORG-thumb_01_R/DEF-thumb_01_R_02
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/ORG-thumb_01_R/ORG-thumb_02_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/ORG-thumb_01_R/ORG-thumb_02_R/DEF-thumb_02_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/ORG-thumb_01_R/ORG-thumb_02_R/ORG-thumb_03_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_01_R/ORG-thumb_01_R/ORG-thumb_02_R/ORG-thumb_03_R/DEF-thumb_03_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_02_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_02_R/DEF-f_middle_01_R_01
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_02_R/DEF-palm_02_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_02_R/ORG-f_middle_01_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_02_R/ORG-f_middle_01_R/DEF-f_middle_01_R_02
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_02_R/ORG-f_middle_01_R/ORG-f_middle_02_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_02_R/ORG-f_middle_01_R/ORG-f_middle_02_R/DEF-f_middle_02_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_02_R/ORG-f_middle_01_R/ORG-f_middle_02_R/ORG-f_middle_03_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_02_R/ORG-f_middle_01_R/ORG-f_middle_02_R/ORG-f_middle_03_R/DEF-f_middle_03_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_03_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_03_R/DEF-f_ring_01_R_01
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_03_R/DEF-palm_03_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_03_R/ORG-f_ring_01_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_03_R/ORG-f_ring_01_R/DEF-f_ring_01_R_02
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_03_R/ORG-f_ring_01_R/ORG-f_ring_02_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_03_R/ORG-f_ring_01_R/ORG-f_ring_02_R/DEF-f_ring_02_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_03_R/ORG-f_ring_01_R/ORG-f_ring_02_R/ORG-f_ring_03_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_03_R/ORG-f_ring_01_R/ORG-f_ring_02_R/ORG-f_ring_03_R/DEF-f_ring_03_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_04_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_04_R/DEF-f_pinky_01_R_01
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_04_R/DEF-palm_04_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_04_R/ORG-f_pinky_01_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_04_R/ORG-f_pinky_01_R/DEF-f_pinky_01_R_02
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_04_R/ORG-f_pinky_01_R/ORG-f_pinky_02_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_04_R/ORG-f_pinky_01_R/ORG-f_pinky_02_R/DEF-f_pinky_02_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_04_R/ORG-f_pinky_01_R/ORG-f_pinky_02_R/ORG-f_pinky_03_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-palm_04_R/ORG-f_pinky_01_R/ORG-f_pinky_02_R/ORG-f_pinky_03_R/DEF-f_pinky_03_R
    m_Weight: 0
  - m_Path: rig/root/ORG-hips/ORG-thigh_L
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-thigh_L/ORG-shin_L
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-thigh_L/ORG-shin_L/ORG-foot_L
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-thigh_L/ORG-shin_L/ORG-foot_L/ORG-toe_L
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-thigh_R
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-thigh_R/ORG-shin_R
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-thigh_R/ORG-shin_R/ORG-foot_R
    m_Weight: 1
  - m_Path: rig/root/ORG-hips/ORG-thigh_R/ORG-shin_R/ORG-foot_R/ORG-toe_R
    m_Weight: 1
  - m_Path: WGT-chest_001
    m_Weight: 1
  - m_Path: WGT-elbow_hose_L_001
    m_Weight: 1
  - m_Path: WGT-elbow_hose_R_001
    m_Weight: 1
  - m_Path: WGT-elbow_target_ik_L_001
    m_Weight: 1
  - m_Path: WGT-elbow_target_ik_R_001
    m_Weight: 1
  - m_Path: WGT-f_index_01_L_001
    m_Weight: 1
  - m_Path: WGT-f_index_01_R_001
    m_Weight: 1
  - m_Path: WGT-f_index_02_L_001
    m_Weight: 1
  - m_Path: WGT-f_index_02_R_001
    m_Weight: 1
  - m_Path: WGT-f_index_03_L_001
    m_Weight: 1
  - m_Path: WGT-f_index_03_R_001
    m_Weight: 1
  - m_Path: WGT-f_index_L_001
    m_Weight: 1
  - m_Path: WGT-f_index_R_001
    m_Weight: 1
  - m_Path: WGT-f_middle_01_L_001
    m_Weight: 1
  - m_Path: WGT-f_middle_01_R_001
    m_Weight: 1
  - m_Path: WGT-f_middle_02_L_001
    m_Weight: 1
  - m_Path: WGT-f_middle_02_R_001
    m_Weight: 1
  - m_Path: WGT-f_middle_03_L_001
    m_Weight: 1
  - m_Path: WGT-f_middle_03_R_001
    m_Weight: 1
  - m_Path: WGT-f_middle_L_001
    m_Weight: 1
  - m_Path: WGT-f_middle_R_001
    m_Weight: 1
  - m_Path: WGT-f_pinky_01_L_001
    m_Weight: 1
  - m_Path: WGT-f_pinky_01_R_001
    m_Weight: 1
  - m_Path: WGT-f_pinky_02_L_001
    m_Weight: 1
  - m_Path: WGT-f_pinky_02_R_001
    m_Weight: 1
  - m_Path: WGT-f_pinky_03_L_001
    m_Weight: 1
  - m_Path: WGT-f_pinky_03_R_001
    m_Weight: 1
  - m_Path: WGT-f_pinky_L_001
    m_Weight: 1
  - m_Path: WGT-f_pinky_R_001
    m_Weight: 1
  - m_Path: WGT-f_ring_01_L_001
    m_Weight: 1
  - m_Path: WGT-f_ring_01_R_001
    m_Weight: 1
  - m_Path: WGT-f_ring_02_L_001
    m_Weight: 1
  - m_Path: WGT-f_ring_02_R_001
    m_Weight: 1
  - m_Path: WGT-f_ring_03_L_001
    m_Weight: 1
  - m_Path: WGT-f_ring_03_R_001
    m_Weight: 1
  - m_Path: WGT-f_ring_L_001
    m_Weight: 1
  - m_Path: WGT-f_ring_R_001
    m_Weight: 1
  - m_Path: WGT-foot_fk_L_001
    m_Weight: 1
  - m_Path: WGT-foot_fk_R_001
    m_Weight: 1
  - m_Path: WGT-foot_ik_L_001
    m_Weight: 1
  - m_Path: WGT-foot_ik_R_001
    m_Weight: 1
  - m_Path: WGT-foot_roll_ik_L_001
    m_Weight: 1
  - m_Path: WGT-foot_roll_ik_R_001
    m_Weight: 1
  - m_Path: WGT-forearm_fk_L_001
    m_Weight: 1
  - m_Path: WGT-forearm_fk_R_001
    m_Weight: 1
  - m_Path: WGT-forearm_hose_end_L_001
    m_Weight: 1
  - m_Path: WGT-forearm_hose_end_R_001
    m_Weight: 1
  - m_Path: WGT-forearm_hose_L_001
    m_Weight: 1
  - m_Path: WGT-forearm_hose_R_001
    m_Weight: 1
  - m_Path: WGT-hand_fk_L_001
    m_Weight: 1
  - m_Path: WGT-hand_fk_R_001
    m_Weight: 1
  - m_Path: WGT-hand_ik_L_001
    m_Weight: 1
  - m_Path: WGT-hand_ik_R_001
    m_Weight: 1
  - m_Path: WGT-head_001
    m_Weight: 1
  - m_Path: WGT-hips_001
    m_Weight: 1
  - m_Path: WGT-knee_hose_L_001
    m_Weight: 1
  - m_Path: WGT-knee_hose_R_001
    m_Weight: 1
  - m_Path: WGT-knee_target_ik_L_001
    m_Weight: 1
  - m_Path: WGT-knee_target_ik_R_001
    m_Weight: 1
  - m_Path: WGT-neck_001
    m_Weight: 1
  - m_Path: WGT-palm_L_001
    m_Weight: 1
  - m_Path: WGT-palm_R_001
    m_Weight: 1
  - m_Path: WGT-root_001
    m_Weight: 1
  - m_Path: WGT-shin_fk_L_001
    m_Weight: 1
  - m_Path: WGT-shin_fk_R_001
    m_Weight: 1
  - m_Path: WGT-shin_hose_end_L_001
    m_Weight: 1
  - m_Path: WGT-shin_hose_end_R_001
    m_Weight: 1
  - m_Path: WGT-shin_hose_L_001
    m_Weight: 1
  - m_Path: WGT-shin_hose_R_001
    m_Weight: 1
  - m_Path: WGT-shoulder_L_001
    m_Weight: 1
  - m_Path: WGT-shoulder_R_001
    m_Weight: 1
  - m_Path: WGT-spine_001
    m_Weight: 1
  - m_Path: WGT-thigh_fk_L_001
    m_Weight: 1
  - m_Path: WGT-thigh_fk_R_001
    m_Weight: 1
  - m_Path: WGT-thigh_hose_end_L_001
    m_Weight: 1
  - m_Path: WGT-thigh_hose_end_R_001
    m_Weight: 1
  - m_Path: WGT-thigh_hose_L_001
    m_Weight: 1
  - m_Path: WGT-thigh_hose_R_001
    m_Weight: 1
  - m_Path: WGT-thumb_01_L_001
    m_Weight: 1
  - m_Path: WGT-thumb_01_R_001
    m_Weight: 1
  - m_Path: WGT-thumb_02_L_001
    m_Weight: 1
  - m_Path: WGT-thumb_02_R_001
    m_Weight: 1
  - m_Path: WGT-thumb_03_L_001
    m_Weight: 1
  - m_Path: WGT-thumb_03_R_001
    m_Weight: 1
  - m_Path: WGT-thumb_L_001
    m_Weight: 1
  - m_Path: WGT-thumb_R_001
    m_Weight: 1
  - m_Path: WGT-toe_L_001
    m_Weight: 1
  - m_Path: WGT-toe_R_001
    m_Weight: 1
  - m_Path: WGT-torso_001
    m_Weight: 1
  - m_Path: WGT-upper_arm_fk_L_001
    m_Weight: 1
  - m_Path: WGT-upper_arm_fk_R_001
    m_Weight: 1
  - m_Path: WGT-upper_arm_hose_end_L_001
    m_Weight: 1
  - m_Path: WGT-upper_arm_hose_end_R_001
    m_Weight: 1
  - m_Path: WGT-upper_arm_hose_L_001
    m_Weight: 1
  - m_Path: WGT-upper_arm_hose_R_001
    m_Weight: 1
  - m_Path: WGT-VIS-foot_ik_L_001
    m_Weight: 1
  - m_Path: WGT-VIS-foot_ik_R_001
    m_Weight: 1
  - m_Path: WGT-VIS-hand_ik_L_001
    m_Weight: 1
  - m_Path: WGT-VIS-hand_ik_R_001
    m_Weight: 1
  - m_Path: WGT-VIS-thigh_pole_ik_L_001
    m_Weight: 1
  - m_Path: WGT-VIS-thigh_pole_ik_R_001
    m_Weight: 1
  - m_Path: WGT-VIS-upper_arm_pole_ik_L_001
    m_Weight: 1
  - m_Path: WGT-VIS-upper_arm_pole_ik_R_001
    m_Weight: 1
